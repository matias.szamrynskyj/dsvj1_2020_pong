#include "Ball.h"
#include <iostream>

namespace pong {

	namespace ball {

		Ball::Ball()
			:ballPosition{ fixedBallPosition }, ballSpeed{ fixedBballSpeed }, ballRadius{ fixedRadius }, ballColor{ fixedColor }{
			top = { 0,0 };
			bottom = { 0,0 };
		}

		Ball::Ball(Vector2 speed, Color color)
			: ballPosition{ fixedBallPosition }, ballSpeed{ speed }, ballRadius{ fixedRadius }, ballColor{ color }{
			top = { 0,0 };
			bottom = { 0,0 };
		}

		Ball::Ball(Vector2 speed, float radius, Color color)
			: ballPosition{ fixedBallPosition }, ballSpeed{ speed }, ballRadius{ radius }, ballColor{ color }{
			top = { 0,0 };
			bottom = { 0,0 };
		}

		Ball::~Ball() {
			std::cout << "Se borro la pelota" << std::endl;
		}

		void Ball::setBallSpeed(Vector2 speed) {
			ballSpeed = speed;
		}

		void Ball::setRadius(float radius) {
			ballRadius = radius;
		}

		void Ball::movement() {
			ballPosition.x += ballSpeed.x;
			ballPosition.y += ballSpeed.y;
		}

		void Ball::setCollisionX() {
			ballSpeed.x *= -1.0f;
		}

		void Ball::setCollisionY() {
			ballSpeed.y *= -1.0f;
		}

		void Ball::setColor(Color color) {
			ballColor = color;
		}

		void Ball::setNewPos(Vector2 pos) {
			ballPosition = pos;
		}

		Vector2 Ball::getXY() {
			return ballPosition;
		}

		Vector2 Ball::getOriginalPos() {
			return fixedBallPosition;
		}

		Vector2 Ball::getTop(Ball* gameBall) {
			return top = { gameBall->getX(), (gameBall->getY() + gameBall->getRadius()) };
		}

		Vector2 Ball::getBottom(Ball* gameBall) {
			return bottom = { gameBall->getX(), (gameBall->getY() - gameBall->getRadius()) };
		}

		float Ball::getRadius() {
			return ballRadius;
		}

		float Ball::getX() {
			return ballPosition.x;
		}

		float Ball::getY() {
			return ballPosition.y;
		}

		float Ball::getSpeedX() {
			return ballSpeed.x;
		}

		float Ball::getSpeedY() {
			return ballSpeed.y;
		}

		void Ball::drawBall() {
			DrawCircleV(ballPosition, ballRadius, ballColor);
		}

		void Ball::resetInGameBall() {
			if (ballPosition.x >= (GetScreenWidth() + ballRadius) || (ballPosition.x + ballRadius <= 0)) {
				ballPosition = fixedBallPosition;
			}
		}

		void Ball::resetBall() {
			ballPosition = fixedBallPosition;
			ballSpeed = fixedBballSpeed;
			ballRadius = fixedRadius;
		}

		float ballSpeedX;
		float ballSpeedY;
		Vector2 ballSpeed;
		Color ballColor;
		Ball* gameBall;

		void init() {
			ballSpeedX = 9.0f;
			ballSpeedY = 9.0f;
			ballSpeed = { ballSpeedX ,ballSpeedY };
			ballColor = BEIGE;
			gameBall = new Ball(ballSpeed, ballColor);
		}

		void update() {

		}

		void draw() {

		}

		void deinit() {
			delete gameBall;
		}
	}
}