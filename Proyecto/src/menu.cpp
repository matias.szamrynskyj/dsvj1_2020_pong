#include "Menu.h"
#include "Buttons.h"

using namespace pong;
using namespace screens;
using namespace buttons;

namespace pong {

	namespace menu {

		//Tama�o botones
		float buttonWidth;
		float buttonHeight;

		float yesNoHeight;
		float yesNoWidth;

		float prevNextHeight;
		float prevNextWidth;

		float playerButtonSizeHeight;
		float playerButtonSizeWidth;

		//Posicion en pantalla
		float buttonMiddleX;
		float buttonMiddleY;

		float pongLogoPosX;
		float pongLogoPosY;

		float playPosX;
		float playPosY;

		float tutorialPosX;
		float tutorialPosY;

		float creditsPosX;
		float creditsPosY;

		float exitPosX;
		float exitPosY;

		float exitPausePosX;
		float exitPausePosY;

		float yesPosX;
		float yesPosY;

		float noPosX;
		float noPosY;

		float backPosX;
		float backPosY;

		float backMenuX;
		float backMenuY;

		float previousPosX;
		float previousPosY;

		float nextPosX;
		float nextPosY;

		float playerSelectionX;
		float playerSelectionY;

		float p1UpPosX;
		float p1UpPosY;

		float p1DownPosX;
		float p1DownPosY;

		float p2UpPosX;
		float p2UpPosY;

		float p2DownPosX;
		float p2DownPosY;

		//Tama�o de la fuente
		float playFontSize;
		float tutorialFontSize;
		float creditsFontSize;
		float quitFontSize;
		float yesFontSize;
		float noFontSize;
		float backFontSize;
		float arrowFontSize;
		float p1Font;
		float playerKeyFont;

		//Texto en los cuadrados
		char playText[5] = "PLAY";
		char tutorialText[9] = "TUTORIAL";
		char creditsText[8] = "CREDITS";
		char exitText[5] = "EXIT";
		char backText[5] = "BACK";
		char yesText[4] = "YES";
		char noText[3] = "NO";
		char previousSimbol[2] = "<";
		char nextSimbol[2] = ">";
		char p1Text[3] = "P1";
		char p2Text[3] = "P2";
		char nextText[5] = "Next";
		char p1UpText[2] = "W";
		char p1DownText[2] = "S";
		char p2UpText[13] = "  Up\nArrow";
		char p2DownText[14] = " Down\nArrow";


		Vector2 buttonSize;
		Vector2 buttonSize2;
		Vector2 buttonSize3;
		Vector2 playerSelectButtonSize;
		Vector2 playerButtonSize;

		//Posicion de los textos dentro de los carteles
		Vector2 playTextPos;
		Vector2 tutorialTextPos;
		Vector2 creditsTextPos;
		Vector2 exitTextPos;
		Vector2 exitTextPausePos;
		Vector2 yesTextPos;
		Vector2 noTextPos;
		Vector2 backTextPos;
		Vector2 previousSimbolPos;
		Vector2 nextSimbolPos;
		Vector2 pongTextPos;
		Vector2 p1SelectTextPos;
		Vector2 p2SelectTextPos;
		Vector2 p1UpKeyTextPos;
		Vector2 p1DownKeyTextPos;
		Vector2 p2UpKeyTextPos;
		Vector2 p2DownKeyTextPos;

		//Posicion del boton exit en menu pausa
		Vector2 exitPausePos;

		Color collisionColor;
		Color defaultColor;
		Color textColor;

		Font font;

		void init() {
			//inicializar todas las variables
			buttonWidth = 300.0f;
			buttonHeight = 105.0f;

			yesNoHeight = 140.0f;
			yesNoWidth = 140.0f;

			prevNextHeight = 50.0f;
			prevNextWidth = 40.0f;

			playerButtonSizeHeight = 100.0f;
			playerButtonSizeWidth = 100.0f;

			buttonMiddleX = static_cast<float>(GetScreenWidth()) / 2 - buttonWidth / 2;
			buttonMiddleY = static_cast<float>(GetScreenHeight()) / 2 - buttonWidth / 2;

			pongLogoPosX = static_cast<float>(GetScreenWidth()) / 2 - 55;
			pongLogoPosY = 100.0f;

			playPosX = buttonMiddleX;
			playPosY = 30.0f;

			tutorialPosX = buttonMiddleX;
			tutorialPosY = 165.0f;

			creditsPosX = buttonMiddleX;
			creditsPosY = 300.0f;

			exitPosX = buttonMiddleX;
			exitPosY = 435.0f;

			exitPausePosX = buttonMiddleX;
			exitPausePosY = 200.0f;

			yesPosX = 160.0f;
			yesPosY = 220.0f;

			noPosX = 500.0f;
			noPosY = 220.0f;

			backPosX = 600.0f;
			backPosY = 510.0f;

			backMenuX = 616.0f;
			backMenuY = 530.0f;

			previousPosX = 300.0f;
			previousPosY = 230.0f;

			nextPosX = 460.0f;
			nextPosY = 230.0f;

			playerSelectionX = 470.0f;
			playerSelectionY = 80.0f;

			p1UpPosX = 100;
			p1UpPosY = 180;

			p1DownPosX = 100;
			p1DownPosY = p1UpPosY + 200;

			p2UpPosX = 300;
			p2UpPosY = p1UpPosY;

			p2DownPosX = 300;
			p2DownPosY = p1DownPosY;

			playFontSize = 105.0f;
			tutorialFontSize = 50.0f;
			creditsFontSize = 58.0f;
			quitFontSize = 105.0f;
			yesFontSize = 60.0f;
			noFontSize = 90.0f;
			backFontSize = 58.0f;
			arrowFontSize = 40.0f;
			p1Font = 30.0f;

			buttonSize = { buttonWidth ,buttonHeight };
			buttonSize2 = { yesNoWidth ,yesNoHeight };
			buttonSize3 = { prevNextWidth ,prevNextHeight };
			playerSelectButtonSize = { prevNextWidth ,prevNextHeight - 5 };
			playerButtonSize = { playerButtonSizeWidth, playerButtonSizeHeight };


			//Posicion de los textos dentro de los carteles-------------------
			playTextPos = { playPosX + 18, playPosY + 5 };
			tutorialTextPos = { tutorialPosX + 5, tutorialPosY + 30 };
			creditsTextPos = { creditsPosX + 10 , creditsPosY + 30 };
			exitTextPos = { exitPosX + 25, exitPosY + 7 };
			exitTextPausePos = { exitPausePosX + 20, exitPausePosY + 5 };
			yesTextPos = { yesPosX + 5, yesPosY + 42 };
			noTextPos = { noPosX + 10, noPosY + 32 };
			backTextPos = { backMenuX, backMenuY };
			previousSimbolPos = { previousPosX + 14, previousPosY + 8 };
			nextSimbolPos = { nextPosX + 14, nextPosY + 8 };
			pongTextPos = { pongLogoPosX, pongLogoPosY };
			p1SelectTextPos = { playerSelectionX + 5, playerSelectionY + 10 };
			p2SelectTextPos = { playerSelectionX + 2 , playerSelectionY + 10 };
			p1UpKeyTextPos = { p1UpPosX + 25 , p1UpPosY + 15 };
			p1DownKeyTextPos = { p1DownPosX + 28, p1DownPosY + 18 };
			p2UpKeyTextPos = { p2UpPosX + 12, p2UpPosY + 17 };
			p2DownKeyTextPos = { p2DownPosX + 12 , p2DownPosY + 16 };
			//----------------------------------------------------------------

			exitPausePos = { exitPausePosX, exitPausePosY };

			collisionColor = RED;
			defaultColor = PINK;
			textColor = BLUE;

			font = GetFontDefault();
		}

		void update() {
			playButton->buttonLogic4(playButton, collisionColor, defaultColor, currentScene, playScreen);
			tutorialButton->buttonLogic4(tutorialButton, collisionColor, defaultColor, currentScene, optionScreen);
			creditsButton->buttonLogic4(creditsButton, collisionColor, defaultColor, currentScene, creditsScreen);
			exitButton->buttonLogic4(exitButton, collisionColor, defaultColor, currentScene, exitScreen);
		}

		void draw() {
			EnableCursor();

			playButton->drawButton(playButton->getColors());
			playButton->showText(font, playText, playTextPos, playFontSize, 8.0f, textColor);

			tutorialButton->drawButton(tutorialButton->getColors());
			tutorialButton->showText(font, tutorialText, tutorialTextPos, tutorialFontSize, 8.0f, textColor);

			creditsButton->drawButton(creditsButton->getColors());
			creditsButton->showText(font, creditsText, creditsTextPos, creditsFontSize, 8.0f, textColor);

			exitButton->drawButton(exitButton->getColors());
			exitButton->showText(font, exitText, exitTextPos, quitFontSize, 8.0f, textColor);
		}
		void deinit() {

		}
	}

}



