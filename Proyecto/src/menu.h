#ifndef MENU_H
#define MENU_H

#include "raylib.h"
#include "screens.h"

namespace pong {

	namespace menu {
		//Ancho de los botones del juego
		extern float buttonWidth;
		extern float buttonHeight;

		//Tama�o botones
		extern float yesNoHeight;    //Altura de los botones Yes/No en pantalla antes de salir
		extern float yesNoWidth;     //Ancho de los botones Yes/No en pantalla antes de salir

		extern float prevNextHeight;
		extern float prevNextWidth;

		extern float playerButtonSizeHeight;
		extern float playerButtonSizeWidth;

		//Posic pantalla
		extern float buttonMiddleX;  //Posici�n del boton a la mitad de pantalla en X
		extern float buttonMiddleY;   //Posici�n del boton a la mitad de pantalla en Y

		extern float pongLogoPosX;    //Posicion del texto Pong en la pantalla de pausa en X
		extern float pongLogoPosY;    //Posicion del texto Pong en la pantalla de pausa en Y

		extern float playPosX;         //Posicion del bot�n Play en la pantalla en X
		extern float playPosY;         //Posicion del bot�n Play en la pantalla en Y

		extern float tutorialPosX;       //Posicion del bot�n Option en la pantalla en X
		extern float tutorialPosY;       //Posicion del bot�n Option en la pantalla en Y

		extern float creditsPosX;     //Posicion del bot�n Tutorial en la pantalla en X
		extern float creditsPosY;     //Posicion del bot�n Tutorial en la pantalla en Y

		extern float exitPosX;     //Posicion del bot�n Exit en la pantalla en X
		extern float exitPosY;     //Posicion del bot�n Exit en la pantalla en Y

		extern float exitPausePosX;    //Posicion del bot�n Exit en la pantalla de pausa en X
		extern float exitPausePosY;    //Posicion del bot�n Exit en la pantalla de pausa en Y

		extern float yesPosX;    //Posicion del bot�n YES en la pantalla en X
		extern float yesPosY;    //Posicion del bot�n YES en la pantalla en Y

		extern float noPosX;     //Posicion del bot�n NO en la pantalla en X
		extern float noPosY;     //Posicion del bot�n NO en la pantalla en Y

		extern float backPosX;   //Posicion del bot�n Back en la pantalla en X
		extern float backPosY;   //Posicion del bot�n Back en la pantalla en Y

		extern float backMenuX;  //Altura del texto Back en X
		extern float backMenuY;  //Altura del texto Back en Y

		extern float previousPosX;
		extern float previousPosY;

		extern float nextPosX;
		extern float nextPosY;

		extern float playerSelectionX;
		extern float playerSelectionY;

		extern float p1UpPosX;
		extern float p1UpPosY;

		extern float p1DownPosX;
		extern float p1DownPosY;

		extern float p2UpPosX;
		extern float p2UpPosY;

		extern float p2DownPosX;
		extern float p2DownPosY;

		//Tama�o de la fuente
		extern float playFontSize;
		extern float tutorialFontSize;
		extern float creditsFontSize;
		extern float quitFontSize;
		extern float yesFontSize;
		extern float noFontSize;
		extern float backFontSize;
		extern float arrowFontSize;
		extern float p1Font;
		extern float playerKeyFont;

		//Texto en los cuadrados
		extern char playText[5];
		extern char tutorialText[9];
		extern char creditsText[8];
		extern char exitText[5];
		extern char backText[5];
		extern char yesText[4];
		extern char noText[3];
		extern char previousSimbol[2];
		extern char nextSimbol[2];
		extern char p1Text[3];
		extern char p2Text[3];
		extern char nextText[5];
		extern char p1UpText[2];
		extern char p1DownText[2];
		extern char p2UpText[13];
		extern char p2DownText[14];

		extern Vector2 buttonSize;
		extern Vector2 buttonSize2;
		extern Vector2 buttonSize3;
		extern Vector2 playerSelectButtonSize;
		extern Vector2 playerButtonSize;

		//Posicion de los textos dentro de los carteles
		extern Vector2 playTextPos;
		extern Vector2 tutorialTextPos;
		extern Vector2 creditsTextPos;
		extern Vector2 exitTextPos;
		extern Vector2 exitTextPausePos;
		extern Vector2 yesTextPos;
		extern Vector2 noTextPos;
		extern Vector2 backTextPos;
		extern Vector2 previousSimbolPos;
		extern Vector2 nextSimbolPos;
		extern Vector2 pongTextPos;
		extern Vector2 p1SelectTextPos;
		extern Vector2 p2SelectTextPos;
		extern Vector2 p1UpKeyTextPos;
		extern Vector2 p1DownKeyTextPos;
		extern Vector2 p2UpKeyTextPos;
		extern Vector2 p2DownKeyTextPos;

		//Posicion del boton exit en menu pausa
		extern Vector2 exitPausePos;

		extern Color collisionColor;
		extern Color defaultColor;
		extern Color textColor;

		extern Font font;

		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif