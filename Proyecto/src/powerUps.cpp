#include "PowerUps.h"

namespace pong {

	namespace powerups {

		PowerUps::PowerUps() {

		}

		PowerUps::~PowerUps() {

		}

		void PowerUps::setShield() {

		}

		void PowerUps::setPaddle(Rectangle& _racket) {
			_racket.height = 300.0f;
		}

		void PowerUps::setSpeed(float& _speed) {
			speed = _speed;
		}


		int PowerUps::timer() {
			return 0;
		}

		int PowerUps::selector() {
			return 0;
		}

		float powerUpPosX;
		float powerUpPosY;

		Vector2 shieldPos;
		Vector2 paddlePos;
		Vector2 velocityPos;

		Vector2 powerUpSize;
		Color shieldColor;
		Color paddleColor;
		Color velocityColor;

		char shieldText[2] = "S";
		char paddleText[2] = "P";
		char velocityText[2] = "V";

		Vector2 shieldTextPos;
		Vector2 paddleTextPos;
		Vector2 velocityTextPos;

		float powerUpFont;

		Buttons* shieldPowerUp;
		Buttons* paddlePowerUp;
		Buttons* velocityPowerUp;

		void init() {
			powerUpPosX = static_cast<float>(GetRandomValue(300, 600));
			powerUpPosY = static_cast<float>(GetRandomValue(200, 500));

			powerUpSize = { 40.0f, 40.0f };
			shieldColor = BLUE;
			paddleColor = RED;
			velocityColor = YELLOW;

			powerUpFont = 40.0f;

			shieldPowerUp = new Buttons(powerUpPosX, powerUpPosY, powerUpSize, shieldColor);
			paddlePowerUp = new Buttons(powerUpPosX, powerUpPosY, powerUpSize, paddleColor);
			velocityPowerUp = new Buttons(powerUpPosX, powerUpPosY, powerUpSize, velocityColor);


			//Posicion de los textos dentro de los carteles-------------------
			shieldTextPos = { powerUpPosX , powerUpPosY };
			paddleTextPos = { powerUpPosX , powerUpPosY };
			velocityTextPos = { powerUpPosX , powerUpPosY };
			//----------------------------------------------------------------

			shieldPowerUp->setName("Shield PowerUp");
			paddlePowerUp->setName("Paddle PowerUp");
			velocityPowerUp->setName("Velocity PowerUp");
		}

		void update() {

		}

		void draw() {

		}

		void deinit() {
			delete shieldPowerUp;
			delete paddlePowerUp;
			delete velocityPowerUp;
		}
	}
}