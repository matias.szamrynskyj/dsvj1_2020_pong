#include "quit.h"

#include "Buttons.h"
#include "Menu.h"
#include "screens.h"
#include "game.h"

using namespace pong;
using namespace buttons;
using namespace menu;
using namespace screens;
using namespace game;


namespace pong {

	namespace quit {
		void init()
		{

		}
		void update()
		{
			//Yes button
			yesButton->buttonLogic1(yesButton, collisionColor, defaultColor, exitGame);

			//No button
			noButton->buttonLogic4(noButton, collisionColor, defaultColor, currentScene, menuScreen);
		}
		void draw()
		{
			yesButton->drawButton(yesButton->getColors());
			yesButton->showText(font, yesText, yesTextPos, yesFontSize, 10.0f, textColor);

			noButton->drawButton(noButton->getColors());
			noButton->showText(font, noText, noTextPos, noFontSize, 10.0f, textColor);
		}
		void deinit()
		{

		}
	}
}
