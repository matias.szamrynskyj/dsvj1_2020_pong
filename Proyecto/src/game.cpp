#include "game.h"
#include "Ball.h"
#include "Player.h"
#include "Buttons.h"
#include "credits.h"
#include "tutorial.h"
#include "play.h"
#include "PowerUps.h"
#include "quit.h"

#include "raylib.h"
using namespace pong;
using namespace screens;
using namespace game;

namespace pong {

	namespace game {

		const int screenWidth = 800;
		const int screenHeight = 600;
		int maxPointSelected = 0;
		bool exitGame = false;


		void run() {
			init();

			while (!WindowShouldClose() && exitGame != true)
			{
				update();
				draw();
			}

			deinit();
		}

		static void init() {
			InitWindow(screenWidth, screenHeight, "PONG");

			SetTargetFPS(60);
			SetExitKey(KEY_DELETE);

			currentScene = Scene::menu;
			maxPointSelected = static_cast<int>(MaxPoints::five);
			exitGame = false;

			menu::init();
			//gameplay::init(); //vacio
			ball::init();
			player::init();
			buttons::init();
			play::init();
			screens::init();
			tutorial::init();
			//credits::init(); //vacio
			//options::init(); //vacio
			powerups::init();
			//quit::init();
		}

		static void update() {
			switch (currentScene)
			{
			case Scene::menu:
				menu::update();
				break;

			case Scene::play:
				play::update();
				break;

			case Scene::tutorial:
				tutorial::update();
				break;

			case Scene::credits:
				credits::update();
				break;

			case Scene::quit:
				quit::update();
				break;
			}
		}

		static void draw() {
			BeginDrawing();
			ClearBackground(BLACK);

			switch (currentScene)
			{
			case Scene::menu:
				menu::draw();
				break;

			case Scene::play:
				play::draw();
				break;

			case Scene::tutorial:
				tutorial::draw();
				break;

			case Scene::credits:
				credits::draw();
				break;

			case Scene::quit:
				quit::draw();
				break;
			}
			EndDrawing();
		}

		static void deinit() {
			CloseWindow();
			//menu::deinit(); //vacio
			//gameplay::deinit(); //vacio
			ball::deinit();
			player::deinit();
			buttons::deinit();
			//credits::deinit(); //vacio
			//options::deinit(); //vacio
			//play::deinit(); //vacio
			powerups::deinit();
			//quit::deinit();
		}
	}
}