#ifndef SCREENS_H
#define SCREENS_H

#include "raylib.h"

namespace pong {

	namespace screens {

		enum class Scene {
			menu = 0,
			play,
			tutorial,
			credits,
			pauseScreen,
			quit
		};

		extern Scene currentScene;
		extern Scene menuScreen;
		extern Scene playScreen;
		extern Scene optionScreen;
		extern Scene creditsScreen;
		extern Scene pauseScreen;
		extern Scene exitScreen;

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
