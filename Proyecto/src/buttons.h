#ifndef BUTTONS_H
#define BUTTONS_H

#include "raylib.h"
#include "screens.h"

#include <string>

using namespace pong;
using namespace screens;

namespace pong {

	namespace buttons {

		class Buttons {
		private:
			const float fixedSpacing = 5.0f;
			const bool fixedWordWrap = true;
			const float width = 300;
			const float height = 105;
			const Color myColor = PINK;
			std::string fixedName = "Sin Nombre";

			Rectangle button;
			Vector2 originalPos = { 0, 0 };
			Color color;
			std::string name;

		public:
			Buttons();

			Buttons(float x, float y, Vector2 size, Color color);

			~Buttons();
			void setHeight(float height);					//Setea la altura del bot�n
			void setWidth(float width);						//Setea el ancho del bot�n
			void setInitPos(Vector2 pos);					//Setea la posici�n inicial del boton
			void setOriginalPos();							//Setea el boton a la posici�n original
			void setNewPos(Vector2 pos);							//Setea el boton a la posici�n original
			void setColors(Color& setColor);				//Setea el color del bot�n	
			void setName(std::string name);

			float getHeight();								//Retorna la altura del bot�n
			float getWidth();								//Retorna el ancho del bot�n
			float getX();									//Retorna la posici�n en X del bot�n
			float getY();									//Retorna la posici�n en Y del bot�n
			Rectangle getButton();							//Retorna el boton en s�
			Color getColors();								//Retorna el color actual del bot�n
			float getFixedSpacing();

			void changePos(Vector2 pos);					//Cambia la posici�n del bot�n

			void drawButton(Color color);					//Dibuja el bot�n

			void showText(Font font, const char* text, Vector2 position, float fontSize, float spacing, Color color);     //Muestra el texto que hay arriba del bot�n

			void buttonLogic1(Buttons* button, Color pressed, Color standby, bool& exitGame);    //Detecta si el mouse pas� por arriba del boton y lo cambia de color y sale del juego al apretarlo
			void buttonLogic2(Buttons* button, Color pressed, Color standby, int& changed);
			void buttonLogic3(Buttons* button, Color pressed, Color standby, bool& playing);
			void buttonLogic4(Buttons* button, Color pressed, Color standby, Scene& currentScene, Scene goToScreen);	//Detecta si el mouse pas� por arriba del boton y lo cambia de color y cambia de escena el apretarlo

			void previousButtonLogic(Buttons* button, Color pressed, Color standby, int& CounterP1);
			void nextButtonLogic(Buttons* button, Color pressed, Color standby, int& CounterP2);
		};

		extern Buttons* playButton;
		extern Buttons* tutorialButton;
		extern Buttons* creditsButton;
		extern Buttons* exitButton;
		extern Buttons* yesButton;
		extern Buttons* noButton;
		extern Buttons* backButton;
		extern Buttons* nextButton;
		extern Buttons* previousButton;
		extern Buttons* playerSelectButton;
		extern Buttons* p1UpKey;
		extern Buttons* p1DownKey;
		extern Buttons* p2UpKey;
		extern Buttons* p2DownKey;

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif 