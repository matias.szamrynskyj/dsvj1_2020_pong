#include "Player.h"
#include <iostream>

using namespace pong;


namespace pong {

	namespace player {
		Player::Player()
			:racket{ racket.x = fixedPos.x, racket.y = fixedPos.y, racket.width = fixedSize.x, racket.height = fixedSize.y }, color{ fixedColor }{
		}

		Player::Player(Vector2 pos, Color myColor)
			: racket{ racket.x = pos.x, racket.y = pos.y, racket.width = fixedSize.x, racket.height = fixedSize.y }, color{ myColor }{
		}

		Player::Player(Vector2 pos, Vector2 size, Color myColor)
			: racket{ racket.x = pos.x, racket.y = pos.y, racket.width = size.x , racket.height = size.y }, color{ myColor }{
		}


		Player::~Player() {
			std::cout << "Se borro player" << std::endl;
		}

		void Player::moveUp(int key) {
			if (racket.y > 0)
			{
				if (IsKeyDown(key)) racket.y -= racketSpeed;
			}
		}

		void Player::moveDown(int key) {
			if (racket.y < GetScreenHeight() - racket.height)
			{
				if (IsKeyDown(key)) racket.y += racketSpeed;
			}
		}

		void Player::setHeight(float height) {
			racket.height = height;
		}

		void Player::setWidth(float width) {
			racket.width = width;
		}

		void Player::setInitPos(Vector2 pos) {
			racket.x = pos.x;
			racket.y = pos.y;
		}

		void Player::setPoints() {
			points++;
		}

		void Player::setColor(Color color) {
			color = color;
		}

		void Player::setPowerUp(int random, float& speed) {
			switch (random)
			{
			case 1:
				myPowerUp.setShield();
				break;

			case 2:
				myPowerUp.setPaddle(racket);
				break;

			case 3:
				myPowerUp.setSpeed(speed);
				break;
			}
		}

		float Player::getHeight() {
			return racket.height;
		}

		float Player::getWidth() {
			return racket.width;
		}

		float Player::getX() {
			return racket.x;
		}

		float Player::getY() {
			return racket.y;
		}

		Rectangle Player::getRacket() {
			return racket;
		}

		int Player::getPoints() {
			return points;
		}

		void Player::resetPlayerSettings(float& speed) {
			racket.height = fixedSize.y;
			speed = racketSpeed;
		}

		void Player::resetPoints() {
			points = 0;
		}

		void Player::drawPlayer() {
			DrawRectangleRec(racket, color);
		}


		Vector2 rightSize;
		Vector2 leftSize;

		Vector2 leftRacketPos;
		Vector2 rightRacketPos;

		Vector2 leftRacketClonePos;
		Vector2 rightRacketClonePos;

		float racketWidth;
		float raccketHeight;

		float racketSpeedP1;
		float racketSpeedP2;

		Player* leftRacket;
		Player* rightRacket;

		Player* leftRacketClone;
		Player* rightRacketClone;

		void init() {
			racketWidth = 40;
			raccketHeight = 100;
			racketSpeedP1 = 12.0f;
			racketSpeedP2 = 12.0f;

			rightSize = { racketWidth,  raccketHeight };
			leftSize = { racketWidth,  raccketHeight };

			leftRacketPos = { 0.0f, static_cast<float>(GetScreenHeight()) / 2.0f - 50.0f };
			rightRacketPos = { static_cast<float>(GetScreenWidth()) - 40.0f, GetScreenHeight() / 2.0f - 50.0f };

			leftRacketClonePos = { 230.0f, 200.0f };
			rightRacketClonePos = { 530.0f, 200.0f };

			leftRacket = new Player(leftRacketPos, leftSize, VIOLET);
			rightRacket = new Player(rightRacketPos, rightSize, YELLOW);
			leftRacketClone = new Player(leftRacketClonePos, leftSize, VIOLET);
			rightRacketClone = new Player(rightRacketClonePos, rightSize, YELLOW);
		}

		void update() {

		}

		void draw() {

		}

		void deinit() {
			delete leftRacket;
			delete rightRacket;
			delete leftRacketClone;
			delete rightRacketClone;
		}
	}
}