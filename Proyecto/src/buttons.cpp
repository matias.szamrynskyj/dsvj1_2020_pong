#include "Menu.h"
#include "Buttons.h"
#include <iostream>

using namespace pong;
using namespace menu;


namespace pong {

	namespace buttons {

		Buttons::Buttons()
			:button{ button.x = 0, button.y = 0, button.width = width, button.height = height }, color{ myColor }, name{ fixedName }{
		}

		Buttons::Buttons(float x, float y, Vector2 size, Color color)
			: button{ button.x = x, button.y = y, button.width = size.x, button.height = size.y }, originalPos{ originalPos.x = button.x ,originalPos.y = button.y }, color{ color }{
		}

		Buttons::~Buttons() {
			std::cout << "Se borro " + name << std::endl;
		}

		void Buttons::setHeight(float height) {
			button.height = height;
		}
		void Buttons::setWidth(float width) {
			button.width = width;
		}
		void Buttons::setInitPos(Vector2 pos) {
			button.x = pos.x;
			button.y = pos.y;

			originalPos.x = pos.x;
			originalPos.y = pos.y;
		}
		void Buttons::setOriginalPos() {
			button.x = originalPos.x;
			button.y = originalPos.y;
		}
		void Buttons::setNewPos(Vector2 pos) {
			button.x = pos.x;
			button.y = pos.y;
		}
		void Buttons::setColors(Color& setColor) {
			color = setColor;
		}

		void Buttons::setName(std::string name) {
			name = name;
		}

		float Buttons::getHeight() {
			return button.height;
		}
		float Buttons::getWidth() {
			return button.width;
		}
		float Buttons::getX() {
			return button.x;
		}
		float Buttons::getY() {
			return button.y;
		}
		Rectangle Buttons::getButton() {
			return button;
		}
		Color Buttons::getColors() {
			return color;
		}
		float Buttons::getFixedSpacing() {
			return fixedSpacing;
		}

		void Buttons::changePos(Vector2 pos) {
			button.x = pos.x;
			button.y = pos.y;
		}

		void Buttons::drawButton(Color color) {
			DrawRectangleRec(button, color);
		}

		void Buttons::showText(Font font, const char* text, Vector2 position, float fontSize, float spacing, Color color) {
			DrawTextEx(font, text, position, fontSize, spacing, color);
		}

		void Buttons::buttonLogic1(Buttons* button, Color pressed, Color standby, bool& exitGame) {
			if (CheckCollisionPointRec(GetMousePosition(), button->getButton())) {
				button->setColors(pressed);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					exitGame = true;
				}
			}
			else {
				button->setColors(standby);
			}
		}
		void Buttons::buttonLogic2(Buttons* button, Color pressed, Color standby, int& changed) {
			if (CheckCollisionPointRec(GetMousePosition(), button->getButton())) {
				button->setColors(pressed);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					if (changed == 1) {
						changed = 2;
					}
					else {
						changed = 1;
					}
				}
			}
			else {
				button->setColors(standby);
			}
		}
		void Buttons::buttonLogic3(Buttons* button, Color pressed, Color standby, bool& playing) {
			if (CheckCollisionPointRec(GetMousePosition(), button->getButton())) {
				button->setColors(pressed);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					playing = true;
				}
			}
			else {
				button->setColors(standby);
			}
		}

		void Buttons::buttonLogic4(Buttons* button, Color pressed, Color standby, Scene& currentScene, Scene goToScreen) {
			if (CheckCollisionPointRec(GetMousePosition(), button->getButton())) {
				button->setColors(pressed);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					currentScene = goToScreen;
				}
			}
			else {
				button->setColors(standby);
			}
		}


		void Buttons::previousButtonLogic(Buttons* button, Color pressed, Color standby, int& CounterP1) {
			if (CheckCollisionPointRec(GetMousePosition(), button->getButton())) {
				button->setColors(pressed);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					CounterP1--;
				}
			}
			else {
				button->setColors(standby);
			}
		}

		void Buttons::nextButtonLogic(Buttons* button, Color pressed, Color standby, int& CounterP2) {
			if (CheckCollisionPointRec(GetMousePosition(), button->getButton())) {
				button->setColors(pressed);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					CounterP2++;
				}
			}
			else {
				button->setColors(standby);
			}
		}

		Buttons* playButton;
		Buttons* tutorialButton;
		Buttons* creditsButton;
		Buttons* exitButton;
		Buttons* yesButton;
		Buttons* noButton;
		Buttons* backButton;
		Buttons* nextButton;
		Buttons* previousButton;
		Buttons* playerSelectButton;
		Buttons* p1UpKey;
		Buttons* p1DownKey;
		Buttons* p2UpKey;
		Buttons* p2DownKey;

		void init() {
			playButton = new Buttons(playPosX, playPosY, buttonSize, defaultColor);
			tutorialButton = new Buttons(tutorialPosX, tutorialPosY, buttonSize, defaultColor);
			creditsButton = new Buttons(creditsPosX, creditsPosY, buttonSize, defaultColor);
			exitButton = new Buttons(exitPosX, exitPosY, buttonSize, defaultColor);
			yesButton = new Buttons(yesPosX, yesPosY, buttonSize2, defaultColor);
			noButton = new Buttons(noPosX, noPosY, buttonSize2, defaultColor);
			backButton = new Buttons(backPosX, backPosY, buttonSize, defaultColor);
			previousButton = new Buttons(previousPosX, previousPosY, buttonSize3, defaultColor);
			nextButton = new Buttons(nextPosX, nextPosY, buttonSize3, defaultColor);
			playerSelectButton = new Buttons(playerSelectionX, playerSelectionY, playerSelectButtonSize, defaultColor);
			p1UpKey = new Buttons(p1UpPosX, p1UpPosY, playerButtonSize, defaultColor);
			p1DownKey = new Buttons(p1DownPosX, p1DownPosY, playerButtonSize, defaultColor);
			p2UpKey = new Buttons(p2UpPosX, p2UpPosY, playerButtonSize, defaultColor);
			p2DownKey = new Buttons(p2DownPosX, p2DownPosY, playerButtonSize, defaultColor);

			playButton->setName("playButton");
			tutorialButton->setName("optionButton");
			creditsButton->setName("tutorialButton");
			exitButton->setName("exitButton");
			yesButton->setName("yesButton");
			noButton->setName("noButton");
			backButton->setName("backButton");
			previousButton->setName("previousButton");
			nextButton->setName("nextButton");
			playerSelectButton->setName("playerSelectButton");
			p1UpKey->setName("p1UpMovementKeys");
			p2UpKey->setName("p2UpMovementKeys");
			p1DownKey->setName("p1DownMovementKeys");
			p2DownKey->setName("p2DownMovementKeys");
		}

		void update() {

		}

		void draw() {

		}

		void deinit() {
			delete playButton;
			delete tutorialButton;
			delete creditsButton;
			delete exitButton;
			delete yesButton;
			delete noButton;
			delete backButton;
			delete nextButton;
			delete previousButton;
			delete playerSelectButton;
			delete p1UpKey;
			delete p2UpKey;
			delete p1DownKey;
			delete p2DownKey;
		}
	}
}