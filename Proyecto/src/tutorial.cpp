#include "tutorial.h"

#include "Buttons.h"
#include "Menu.h"
#include "screens.h"

using namespace pong;
using namespace buttons;
using namespace menu;
using namespace screens;

namespace pong {

	namespace tutorial {

		int player1TextPosX;
		int player1TextPosY;

		int player2TextPosX;
		int player2TextPosY;

		int goUpTextPosY;
		int goUpTextPosX;

		int goDownTextPosX;
		int goDownTextPosY;


		int controlsTextPosX;
		int controlsTextPosY;


		void init()
		{
			player1TextPosX = 90;
			player1TextPosY = 115;

			player2TextPosX = 290;
			player2TextPosY = player1TextPosY;

			goUpTextPosX = 425;
			goUpTextPosY = 220;

			goDownTextPosX = goUpTextPosX;
			goDownTextPosY = 420;

			controlsTextPosX = GetScreenWidth() / 2 - 80;
			controlsTextPosY = 30;
		}

		void update()
		{

			backButton->buttonLogic4(backButton, collisionColor, defaultColor, currentScene, menuScreen);
		}

		void draw()
		{
			DrawText(FormatText("CONTROLS"), controlsTextPosX, controlsTextPosY, 30, GREEN);

			DrawText(FormatText("Player 1"), player1TextPosX, player1TextPosY, 30, GREEN);
			DrawText(FormatText("Player 2"), player2TextPosX, player2TextPosY, 30, GREEN);

			DrawText(FormatText("Go Up"), goUpTextPosX, goUpTextPosY, 30, GREEN);
			DrawText(FormatText("Go Down"), goDownTextPosX, goDownTextPosY, 30, GREEN);



			p1UpKey->drawButton(RED);
			p1UpKey->showText(font, p1UpText, p1UpKeyTextPos, 75.0f, 5.0f, BLUE);

			p1DownKey->drawButton(RED);
			p1DownKey->showText(font, p1DownText, p1DownKeyTextPos, 75.0f, 5.0f, BLUE);

			p2UpKey->drawButton(RED);
			p2UpKey->showText(font, p2UpText, p2UpKeyTextPos, 28.0f, 1.0f, BLUE);

			p2DownKey->drawButton(RED);
			p2DownKey->showText(font, p2DownText, p2DownKeyTextPos, 28.0f, 1.0f, BLUE);

			backButton->drawButton(backButton->getColors());
			backButton->showText(font, backText, backTextPos, backFontSize, 10.0f, textColor);
		}

		void deinit()
		{
		}

	}

}