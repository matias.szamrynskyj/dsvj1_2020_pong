#include "credits.h"

#include "Buttons.h"
#include "Menu.h"
#include "screens.h"

using namespace pong;
using namespace buttons;
using namespace menu;
using namespace screens;

namespace pong {

	namespace credits {

		void init()
		{

		}

		void update()
		{
			backButton->buttonLogic4(backButton, collisionColor, defaultColor, currentScene, menuScreen);
		}

		void draw()
		{
			DrawText(FormatText("Game done by: Matias Szamrynskyj"), GetScreenWidth() / 2 - 250, GetScreenHeight() / 2 - 25, 30, GREEN);
			backButton->drawButton(backButton->getColors());
			backButton->showText(font, backText, backTextPos, backFontSize, 10.0f, textColor);
		}

		void deinit()
		{

		}

	}
}
