#include "play.h"
#include <iostream>

using namespace player;
using namespace menu;
using namespace powerups;
using namespace ball;
using namespace buttons;
using namespace screens;
using namespace game;


namespace pong
{
	namespace play {

		bool pause;
		bool playing;
		int colorCounterP1;
		int aux1;
		int colorCounterP2;
		//Activa el timer que cuenta los segundos que el powerUp aparece en pantalla
		bool activateTimerpowerUp;

		//Dice cual fue la ultima raqueta que toco para darle a esa raqueta el power up
		int lastRacketTouched;

		bool isPowerUpActive;

		//Game timer
		int timer;
		int timerSeconds;
		int AuxTimerSeconds;
		int timerMax;

		//En que segundo se activa la logica y se muestra por pantalla el powerUp
		int activationTime;

		//Para poder entrar de nuevo a la funcion de seleccion random de powerUp
		bool puSelected;

		int powerUpSelected;

		bool isActive;

		//Cada cuanto tiempo el powerUp aparece en pantalla
		int powerUpOn;

		bool powerOn;

		bool currentPowerUpOff;

		int timerPU;
		int timerSecondsPU;
		int timerMaxPU;

		//Si se toco
		bool powerUpTouched;

		int gameRounds;
		int maxPointSelected;


		//Tiempo que el powerUp estara activo al agarrarlo
		int powerUpActiveTime;

		//Timer cuando se agarra el powerUp
		int timerPickedPU;
		int timerPickedPUCountDown;
		int timerSecondsPickedPU;
		int timerMaxPickedPU;

		int powerUpShowTimeCountDown;

		bool timerAuxSeconds;

		int changed;

		int colorTextPosX;
		int colorTextPosY;

		int impact;

		float racketSpeedPowerUpP1 = 25.0f;
		float racketSpeedPowerUpP2 = 25.0f;

		void input() {
			leftRacket->moveUp(KEY_W);
			leftRacket->moveDown(KEY_S);

			rightRacket->moveUp(KEY_UP);
			rightRacket->moveDown(KEY_DOWN);
		}

		void gameCollisions() {
			//Check Collision ground
			if ((gameBall->getY() >= (GetScreenHeight() - gameBall->getRadius())) || (gameBall->getY() <= gameBall->getRadius())) {
				gameBall->setCollisionY();
			}


			//Check Collision right rackets
			if (CheckCollisionCircleRec(gameBall->getXY(), gameBall->getRadius(), rightRacket->getRacket())) {
				if (gameBall->getSpeedX() > 0) {
					gameBall->setCollisionX();
				}

				lastRacketTouched = (int)LastRacket::Right;
			}

			//Check Collision left rackets
			if (CheckCollisionCircleRec(gameBall->getXY(), gameBall->getRadius(), leftRacket->getRacket())) {
				if (gameBall->getSpeedX() < 0) {
					gameBall->setCollisionX();
				}

				lastRacketTouched = (int)LastRacket::Left;
			}

			//Collision en Y
			if (CheckCollisionPointRec(gameBall->getTop(gameBall), rightRacket->getRacket()) ||
				CheckCollisionPointRec(gameBall->getBottom(gameBall), rightRacket->getRacket()) && impact != 1)
			{
				impact = 1;
				gameBall->setCollisionY();
			}

			if (CheckCollisionPointRec(gameBall->getTop(gameBall), leftRacket->getRacket()) ||
				CheckCollisionPointRec(gameBall->getBottom(gameBall), leftRacket->getRacket()) && impact != 2)
			{
				impact = 2;
				gameBall->setCollisionY();
			}


			//Setea los puntos al jugador izquierdo a la pelota irse de la pantalla
			if (gameBall->getX() >= GetScreenWidth() + gameBall->getRadius()) {
				leftRacket->setPoints();
			}

			//Setea los puntos al jugador derecho a la pelota irse de la pantalla
			if (gameBall->getX() <= 0 - gameBall->getRadius()) {
				rightRacket->setPoints();
			}
		}

		void setPoints() {
			if (leftRacket->getPoints() == maxPointSelected || rightRacket->getPoints() == maxPointSelected) {
				gameRounds++;
				leftRacket->resetPoints();
				rightRacket->resetPoints();
			}
		}

		void gameTimer() {
			if (timer <= timerMax) {
				timer++;
				if (timer == 60) {
					timerSeconds++;
					AuxTimerSeconds++;

					if (AuxTimerSeconds > activationTime) {
						AuxTimerSeconds = 0;
						activateTimerpowerUp = true;
					}
					timer = 0;
				}
			}
		}

		void powerupTimer() {
			if (activateTimerpowerUp) {
				if (timerPU <= timerMaxPU) {
					timerPU++;
					if (timerPU == 60) {
						timerSecondsPU++;
						powerUpShowTimeCountDown--;
						timerPU = 0;

						if (powerUpShowTimeCountDown == 0) {
							timerSecondsPU = 0;
							activateTimerpowerUp = false;
							powerUpShowTimeCountDown = activationTime;
						}
					}
				}
			}
		}

		void resetGame(Vector2 leftInitPos, Vector2 rightInitPos2, Buttons* button) {
			pause = !pause;

			leftRacket->setHeight(100.0f);
			leftRacket->setWidth(40.0f);
			leftRacket->setInitPos(leftInitPos);
			leftRacket->resetPoints();

			rightRacket->setHeight(100.0f);
			rightRacket->setWidth(40.0f);
			rightRacket->setInitPos(rightInitPos2);
			rightRacket->resetPoints();

			gameBall->resetBall();
			button->setOriginalPos();
		}


		void init() {
			pause = false;
			playing = false;
			colorCounterP1 = 0;
			aux1 = 0;
			colorCounterP2 = 0;
			//Activa el timer que cuenta los segundos que el powerUp aparece en pantalla
			activateTimerpowerUp = false;

			//Dice cual fue la ultima raqueta que toco para darle a esa raqueta el power up
			lastRacketTouched = 0;

			isPowerUpActive = false;

			//Game timer
			timer = 0;
			timerSeconds = 0;
			AuxTimerSeconds = 0;
			timerMax = 60;

			//En que segundo se activa la logica y se muestra por pantalla el powerUp
			activationTime = 6;

			//Para poder entrar de nuevo a la funcion de seleccion random de powerUp
			puSelected = true;


			powerUpSelected = 1;

			isActive = false;

			//Cada cuanto tiempo el powerUp aparece en pantalla
			powerUpOn = 0;

			powerOn = true;

			currentPowerUpOff = false;

			timerPU = 0;
			timerSecondsPU = 0;
			timerMaxPU = 60;

			//Si se toco
			powerUpTouched = false;

			gameRounds = 0;
			maxPointSelected = static_cast<int>(MaxPoints::five);

			//Tiempo que el powerUp estara activo al agarrarlo
			powerUpActiveTime = 3;

			//Timer cuando se agarra el powerUp
			timerPickedPU = 0;
			timerPickedPUCountDown = powerUpActiveTime;
			timerSecondsPickedPU = 0;
			timerMaxPickedPU = 60;

			powerUpShowTimeCountDown = activationTime;

			timerAuxSeconds = false;

			changed = 1;

			colorTextPosX = 275;
			colorTextPosY = 20;

			impact = 0;
		}

		void update() {
			switch (playing) {
			case false:

				playerSelectButton->buttonLogic2(playerSelectButton, collisionColor, defaultColor, changed);

				//Para mostrar luego en pantalla cual color estas eligiendo
				switch (changed)
				{
				case 1:
					aux1 = colorCounterP1;
					break;

				case 2:
					aux1 = colorCounterP2;
					break;
				}

				switch (changed)
				{
				case 1:

					if (colorCounterP1 < 10)
					{
						nextButton->nextButtonLogic(nextButton, collisionColor, defaultColor, colorCounterP1);
					}
					else if (colorCounterP1 == 10)
					{
						nextButton->setColors(defaultColor);
					}

					if (colorCounterP1 > 0)
					{
						previousButton->previousButtonLogic(previousButton, collisionColor, defaultColor, colorCounterP1);
					}
					else if (colorCounterP1 == 0)
					{
						previousButton->setColors(defaultColor);
					}

					break;

				case 2:

					if (colorCounterP2 < 10)
					{
						nextButton->nextButtonLogic(nextButton, collisionColor, defaultColor, colorCounterP2);
					}
					else if (colorCounterP2 == 10)
					{
						nextButton->setColors(defaultColor);
					}

					if (colorCounterP2 > 0)
					{
						previousButton->previousButtonLogic(previousButton, collisionColor, defaultColor, colorCounterP2);
					}
					else if (colorCounterP2 == 0)
					{
						previousButton->setColors(defaultColor);
					}

					break;
				}

				switch (colorCounterP1)
				{
				case 0:
					leftRacket->setColor(BEIGE);
					leftRacketClone->setColor(BEIGE);
					break;

				case 1:
					leftRacket->setColor(ORANGE);
					leftRacketClone->setColor(ORANGE);
					break;

				case 2:
					leftRacket->setColor(RED);
					leftRacketClone->setColor(RED);
					break;

				case 3:
					leftRacket->setColor(GREEN);
					leftRacketClone->setColor(GREEN);
					break;

				case 4:
					leftRacket->setColor(BLUE);
					leftRacketClone->setColor(BLUE);
					break;

				case 5:
					leftRacket->setColor(PURPLE);
					leftRacketClone->setColor(PURPLE);
					break;

				case 6:
					leftRacket->setColor(VIOLET);
					leftRacketClone->setColor(VIOLET);
					break;

				case 7:
					leftRacket->setColor(YELLOW);
					leftRacketClone->setColor(YELLOW);
					break;

				case 8:
					leftRacket->setColor(WHITE);
					leftRacketClone->setColor(WHITE);
					break;

				case 9:
					leftRacket->setColor(MAGENTA);
					leftRacketClone->setColor(MAGENTA);
					break;

				case 10:
					leftRacket->setColor(GRAY);
					leftRacketClone->setColor(GRAY);
					break;
				}

				switch (colorCounterP2)
				{
				case 0:
					rightRacket->setColor(BEIGE);
					rightRacketClone->setColor(BEIGE);
					break;

				case 1:
					rightRacket->setColor(ORANGE);
					rightRacketClone->setColor(ORANGE);
					break;

				case 2:
					rightRacket->setColor(RED);
					rightRacketClone->setColor(RED);
					break;

				case 3:
					rightRacket->setColor(GREEN);
					rightRacketClone->setColor(GREEN);
					break;

				case 4:
					rightRacket->setColor(BLUE);
					rightRacketClone->setColor(BLUE);
					break;

				case 5:
					rightRacket->setColor(PURPLE);
					rightRacketClone->setColor(PURPLE);
					break;

				case 6:
					rightRacket->setColor(VIOLET);
					rightRacketClone->setColor(VIOLET);
					break;

				case 7:
					rightRacket->setColor(YELLOW);
					rightRacketClone->setColor(YELLOW);
					break;

				case 8:
					rightRacket->setColor(WHITE);
					rightRacketClone->setColor(WHITE);
					break;

				case 9:
					rightRacket->setColor(MAGENTA);
					rightRacketClone->setColor(MAGENTA);
					break;

				case 10:
					rightRacket->setColor(GRAY);
					rightRacketClone->setColor(GRAY);
					break;
				}

				//Re-utilizo el backButton y le pongo el texto next
				backButton->buttonLogic3(backButton, collisionColor, defaultColor, playing);
				backButton->drawButton(backButton->getColors());
				backButton->showText(font, nextText, backTextPos, backFontSize, 10.0f, textColor);
				break;

			case true:
				if (IsKeyPressed(KEY_ESCAPE)) {
					pause = !pause;
				}

				if (!pause) {
					//Player Input
					input();

					//Ball Movement
					gameBall->movement();

					gameCollisions();

					setPoints();

					//Reset Ball
					gameBall->resetInGameBall();

					//Timer Partida
					gameTimer();

					/*
					//activationTime
					//si timerSeconds == activationTime, activo la logica y el draw
					if (powerOn)
					{
						activationTime = GetRandomValue(6, 12);
						powerUpShowTime = 5;
						powerUpShowTimeCountDown = powerUpShowTime;
						powerOn = false;
						timerAuxSeconds = true;
					}

					//cuando se salga del if de arriba, tengo que activar un bool que me va a activar otro timer
					//en donde AuxTimerSeconds va a empezar de 0 hasta llegar a activationTime. Y cuando llegue
					//Se va a activar el timer de abajo

					if (timerAuxSeconds)
					{
						if (timer4 <= timerMax4)
						{
							timer4++;
							if (timer4 == 60)
							{
								AuxTimerSeconds++;
								timer4 = 0;

								if (AuxTimerSeconds == activationTime)
								{
									activateTimerpowerUp = true;
									timerAuxSeconds = false;
									AuxTimerSeconds = 0;
								}
							}
						}
					}


					//Timer powerUp
					//Pasan 6 segundos, se activa el timer.
					//Se ejecuta el timer hasta que powerUpShowTimeCountDown sea 0
					//empieza a restar pero cuando el jugador lo agarra, puede desaparecer al instante si no se agarro antes
					if (activateTimerpowerUp)
					{
						if (timerPU <= timerMaxPU)
						{
							timerPU++;
							if (timerPU == 60)
							{
								timerSecondsPU++;
								powerUpShowTimeCountDown--;
								timerPU = 0;

								if (powerUpShowTimeCountDown == 0) {
									timerSecondsPU = 0;
									timeLeftFinished = true;
									activateTimerpowerUp = false;
								}
							}
						}
					}

					//otro timer para que cuando recien agarre el power up, ahi me descuente el tiempo de uso
					if (isPowerUpActive)
					{
						if (timerPickedPU <= timerMaxPickedPU)
						{
							timerPickedPU++;
							if (timerPickedPU == 60)
							{
								timerSecondsPickedPU++;
								timerPickedPUCountDown--;
								timerPickedPU = 0;

								if (timerPickedPUCountDown == 0)
								{
									timerSecondsPickedPU = 0;
									timerPickedPUCountDown = powerUpActiveTime;
									isPowerUpActive = false;
									currentPowerUpOff = true;
								}
							}
						}
					}
					*/

					//empieza a restar y hace desparecer el powerUp si no se agarra a tiempo
					powerupTimer();

					//timer para que cuando recien agarre el power up, ahi me descuente el tiempo de uso
					if (isPowerUpActive) {
						if (timerPickedPU <= timerMaxPickedPU) {
							timerPickedPU++;
							if (timerPickedPU == 60) {
								timerSecondsPickedPU++;
								timerPickedPUCountDown--;
								timerPickedPU = 0;

								if (timerPickedPUCountDown == 0) {
									timerSecondsPickedPU = 0;
									timerPickedPUCountDown = powerUpActiveTime;
									isPowerUpActive = false;
									currentPowerUpOff = true;
								}
							}
						}
					}

					if (AuxTimerSeconds == activationTime) {
						isActive = true;
					}

					//Selecciona uno de los 3 powerUp apenas comienza el juego se setea
					if (puSelected) {
						powerUpSelected = GetRandomValue(2, 3);
					}

					//posiciona de manera random el powerUp en pantalla apenas comienza el juego se setea
					if (puSelected) {
						powerUpPosX = static_cast<float>(GetRandomValue(300, 600));
						powerUpPosY = static_cast<float>(GetRandomValue(200, 500));

						shieldPos = { powerUpPosX , powerUpPosY };
						paddlePos = { powerUpPosX , powerUpPosY };
						velocityPos = { powerUpPosX , powerUpPosY };

						shieldTextPos = { powerUpPosX , powerUpPosY };
						paddleTextPos = { powerUpPosX , powerUpPosY };
						velocityTextPos = { powerUpPosX , powerUpPosY };

						shieldPowerUp->setNewPos(shieldPos);
						paddlePowerUp->setNewPos(paddlePos);
						velocityPowerUp->setNewPos(velocityPos);

						puSelected = false;
					}

					//Se activa la colision entre la pelota y los powerUp
					switch (powerUpSelected) {
						/*
						case 1:
							if (_isActive)
							{
								//Shield
								if (CheckCollisionCircleRec(gameBall->GetXY(), gameBall->GetRadius(), shieldPowerUp->GetButton())) {
									leftRacket->SetPowerUp(powerUpSelected, racketSpeedP1);
									_isActive = false;
								}
							}
							break;
						*/
					case 2:
						if (isActive && powerUpTouched != true && activateTimerpowerUp == true) {
							//paddle
							if (CheckCollisionCircleRec(gameBall->getXY(), gameBall->getRadius(), paddlePowerUp->getButton())) {

								switch (lastRacketTouched) {
								case (int)LastRacket::Left:
									leftRacket->setPowerUp(powerUpSelected, racketSpeedPowerUpP1);
									break;

								case (int)LastRacket::Right:
									rightRacket->setPowerUp(powerUpSelected, racketSpeedPowerUpP2);
									break;
								}
								isActive = false;
								powerUpTouched = true;
								isPowerUpActive = true;
							}
						}
						break;

					case 3:
						if (isActive && powerUpTouched != true && activateTimerpowerUp == true) {
							//velocity
							if (CheckCollisionCircleRec(gameBall->getXY(), gameBall->getRadius(), velocityPowerUp->getButton())) {
								switch (lastRacketTouched) {
								case (int)LastRacket::Left:
									leftRacket->setPowerUp(powerUpSelected, racketSpeedP1);
									break;

								case (int)LastRacket::Right:
									rightRacket->setPowerUp(powerUpSelected, racketSpeedP2);
									break;
								}

								isActive = false;
								powerUpTouched = true;
								isPowerUpActive = true;
							}
						}
						break;
					}

					if (currentPowerUpOff == true) {
						leftRacket->resetPlayerSettings(racketSpeedP1);
						rightRacket->resetPlayerSettings(racketSpeedP2);
						puSelected = true;
						isActive = false;
						powerUpTouched = false;
						currentPowerUpOff = false;
						powerOn = true;
					}
				}
				else {
					exitButton->changePos(exitPausePos);
					exitButton->buttonLogic4(exitButton, collisionColor, defaultColor, currentScene, menuScreen);

					if (currentScene == menuScreen) {
						resetGame(leftRacketPos, rightRacketPos, exitButton);
						playing = false;
						colorCounterP1 = 0;
						colorCounterP2 = 0;
						gameRounds = 0;
						timerSeconds = 0;
					}
				}
				break;
			}
		}

		void draw() {

			switch (playing) {
			case false:

				DrawText(FormatText("Color: %i", aux1 + 1), colorTextPosX, colorTextPosY, 40, GREEN);
				DrawText(FormatText("- %i", 11), colorTextPosX + 180, 20, 40, GREEN);

				std::cout << colorCounterP1 << std::endl;


				DrawText(FormatText("Selected:"), colorTextPosX, 80, 40, GREEN);
				playerSelectButton->drawButton(playerSelectButton->getColors());

				switch (changed) {
				case 1:
					playerSelectButton->showText(font, p1Text, p1SelectTextPos, p1Font, 5.0f, textColor);
					break;

				case 2:
					playerSelectButton->showText(font, p2Text, p2SelectTextPos, p1Font, 2.0f, textColor);
					break;
				}

				previousButton->drawButton(previousButton->getColors());
				previousButton->showText(font, previousSimbol, previousSimbolPos, arrowFontSize, 10.0f, textColor);

				nextButton->drawButton(nextButton->getColors());
				nextButton->showText(font, nextSimbol, nextSimbolPos, arrowFontSize, 10.0f, textColor);

				leftRacketClone->drawPlayer();
				rightRacketClone->drawPlayer();

				backButton->drawButton(backButton->getColors());
				backButton->showText(font, nextText, backTextPos, backFontSize, 10.0f, textColor);

				break;

			case true:
				leftRacket->drawPlayer();
				rightRacket->drawPlayer();
				gameBall->drawBall();

				DrawText(FormatText("P1: %i", leftRacket->getPoints()), 150, 20, 40, GREEN);
				DrawText(FormatText("P2: %i", rightRacket->getPoints()), 550, 20, 40, GREEN);
				DrawText(FormatText("Round: %i", gameRounds), 310, 20, 40, GREEN);

				DrawText(FormatText("Timer: %i", timerSeconds), 310, 60, 30, GREEN);


				if (AuxTimerSeconds >= activationTime || powerUpTouched != true) {
					switch (powerUpSelected) {
						/*
					case 1:

						if (_isActive)
						{
							shieldPowerUp->DrawButton(shieldColor);
							shieldPowerUp->ShowText(font, shieldText, shieldTextPos, 30.0f, 5.0f, GREEN);
						}
						break;
						*/
					case 2:
						if (isActive && powerUpTouched != true && activateTimerpowerUp == true) {
							paddlePowerUp->drawButton(paddleColor);
							paddlePowerUp->showText(font, paddleText, paddleTextPos, 30.0f, 5.0f, GREEN);
						}
						break;

					case 3:
						if (isActive && powerUpTouched != true && activateTimerpowerUp == true) {
							velocityPowerUp->drawButton(velocityColor);
							velocityPowerUp->showText(font, velocityText, velocityTextPos, 30.0f, 5.0f, GREEN);
						}
						break;
					}
				}

				if (!pause) {
					DrawText("PRESS ESCAPE to PAUSE GAME", 10, GetScreenHeight() - 25, 20, textColor);
				}
				else {
					DrawText("PONG", (int)pongLogoPosX, (int)pongLogoPosY, 40, RED);
					exitButton->drawButton(exitButton->getColors());
					exitButton->showText(font, exitText, exitTextPausePos, quitFontSize, 10.0f, textColor);

					DrawText("PRESS ESCAPE to UNPAUSE", 10, GetScreenHeight() - 25, 20, textColor);
				}
				DrawFPS(10, 10);
				break;
			}
		}

		void deinit() {


		}
	}
}