#ifndef GAME_H
#define GAME_H

//include de menu.h, gameplay.h, etc

#include "Menu.h"
#include "Gameplay.h"
#include "screens.h"


namespace pong {

	namespace game {

		extern const int screenWidth;
		extern const int screenHeight;
		extern int maxPointSelected;

		extern bool exitGame;

		enum class MaxPoints {
			five = 5,
			ten = 10,
			fifthteen = 20,
			tweenty_five = 25,
		};

		void run();

		static void init();
		static void update();
		static void draw();
		static void deinit();
	}
}

#endif 
