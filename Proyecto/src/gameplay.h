#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"


namespace pong {

	namespace gameplay{

		void init();
		void update();
		void draw();
		void deinit();

	}

}

#endif


