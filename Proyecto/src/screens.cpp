#include "screens.h"

namespace pong {

	namespace screens {
		Scene currentScene;
		Scene menuScreen;
		Scene playScreen;
		Scene optionScreen;
		Scene creditsScreen;
		Scene pauseScreen;
		Scene exitScreen;

		void init()
		{
			menuScreen = Scene::menu;
			playScreen = Scene::play;
			optionScreen = Scene::tutorial;
			creditsScreen = Scene::credits;
			pauseScreen = Scene::pauseScreen;
			exitScreen = Scene::quit;
		}
		void update()
		{

		}
		void draw()
		{

		}
		void deinit()
		{

		}
	}
}