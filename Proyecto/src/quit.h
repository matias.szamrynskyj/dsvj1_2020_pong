#ifndef QUIT_H
#define QUIT_H

namespace pong {

	namespace quit {

		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif
