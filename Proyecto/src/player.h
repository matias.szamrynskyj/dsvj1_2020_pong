#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"
#include "PowerUps.h"

using namespace pong;
using namespace powerups;

namespace pong {

	namespace player {
		enum class LastRacket {
			Left = 1,
			Right
		};

		class Player {
		private:
			const Vector2 fixedSize = { 40, 100 };
			const Vector2 fixedPos = { 0,0 };
			const Color fixedColor = WHITE;
			float racketSpeed = 15.0f;
			PowerUps myPowerUp;


			Color color;
			Rectangle racket;
			int points = 0;
		public:
			Player();
			Player(Vector2 size, Color myColor);
			Player(Vector2 size, Vector2 pos, Color myColor);
			~Player();

			void moveUp(int key);
			void moveDown(int key);

			void setHeight(float height);
			void setWidth(float width);
			void setInitPos(Vector2 pos);
			void setPoints();
			void setColor(Color color);
			void setPowerUp(int random, float& speed);

			float getHeight();
			float getWidth();
			float getX();
			float getY();
			Rectangle getRacket();
			int getPoints();

			void resetPlayerSettings(float& speed);
			void resetPoints();
			void drawPlayer();
		};

		extern Vector2 rightSize;
		extern Vector2 leftSize;

		extern Vector2 leftRacketPos;
		extern Vector2 rightRacketPos;

		extern Vector2 leftRacketClonePos;
		extern Vector2 rightRacketClonePos;

		extern float racketWidth;    //Setea el ancho de los botones del juego
		extern float raccketHeight;   //Setea el alto de los botones del juego

		extern float racketSpeedP1;                  //Velocidad de la racketa
		extern float racketSpeedP2;

		extern Player* leftRacket;
		extern Player* rightRacket;

		extern Player* leftRacketClone;
		extern Player* rightRacketClone;

		void init();
		void update();
		void draw();
		void deinit();
	}

}
#endif