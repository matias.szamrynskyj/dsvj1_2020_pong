#ifndef OPTIONS_H
#define OPTIONS_H

namespace pong {

	namespace tutorial {

		extern int player1TextPosX;
		extern int player1TextPosY;

		extern int player2TextPosX;
		extern int player2TextPosY;

		extern int goUpTextPosX;
		extern int goUpTextPosY;

		extern int goDownTextPosX;
		extern int goDownTextPosY;

		extern int controlsTextPosX;
		extern int controlsTextPosY;


		void init();
		void update();
		void draw();
		void deinit();
	}

}

#endif

