#ifndef POWERUPS_H
#define POWERUPS_H

#include "raylib.h"
#include "Buttons.h"

using namespace pong;
using namespace buttons;


namespace pong {

	namespace powerups {

		class PowerUps {
		private:
			int shield = 1;
			int bigPaddle = 2;
			float speed = 25.0f;

		public:
			PowerUps();
			~PowerUps();

			void setShield();
			void setPaddle(Rectangle& _racket);
			void setSpeed(float& _speed);

			int timer();
			int selector();
		};

		extern float powerUpPosX;
		extern float powerUpPosY;

		extern Vector2 shieldPos;
		extern Vector2 paddlePos;
		extern Vector2 velocityPos;

		extern Vector2 powerUpSize;
		extern Color shieldColor;
		extern Color paddleColor;
		extern Color velocityColor;

		extern char shieldText[2];
		extern char paddleText[2];
		extern char velocityText[2];

		extern Vector2 shieldTextPos;
		extern Vector2 paddleTextPos;
		extern Vector2 velocityTextPos;

		extern float powerUpFont;

		extern Buttons* shieldPowerUp;
		extern Buttons* paddlePowerUp;
		extern Buttons* velocityPowerUp;

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif