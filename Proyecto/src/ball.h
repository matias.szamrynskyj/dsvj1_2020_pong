#ifndef BALL_H
#define BALL_H

#include "raylib.h"

namespace pong {

	namespace ball {

		class Ball
		{
		private:
			const Vector2 fixedBallPosition = { (float)GetScreenWidth() / 2, (float)GetScreenHeight() / 2 };
			const Vector2 fixedBballSpeed = { 9.0f, 9.0f };
			const float fixedRadius = 20.0f;
			const Color fixedColor = RED;

			Vector2 top;
			Vector2 bottom;

			Vector2 ballPosition;
			Vector2 ballSpeed;
			float ballRadius;
			Color ballColor;

		public:
			Ball();
			Ball(Vector2 speed, Color color);
			Ball(Vector2 speed, float radius, Color color);
			~Ball();

			void setBallSpeed(Vector2 speed);                      //Setea la velocidad de la pelota
			void setRadius(float radius);                          //Setea el radio de la pelota
			void movement();                                       //Ejecuta el movimiento de la pelota
			void setCollisionX();                                  //Colision y cambio de direccion en X
			void setCollisionY();                                  //Colision y cambio de direccion en Y
			void setColor(Color color);                            //Seteo el color de la pelota
			void setNewPos(Vector2 pos);                            //Seteo el color de la pelota

			float getRadius();                                     //Retorna el radio de la pelota
			float getX();                                          //Retorna la posicion en X de la pelota
			float getY();                                          //Retorna la posicion en Y de la pelota
			float getSpeedX();                                     //Retorna la velocidad en X de la pelota
			float getSpeedY();                                     //Retorna la velocidad en X de la pelota
			Vector2 getXY();                                       //Retorna la posicion en X e Y de la pelota
			Vector2 getOriginalPos();
			Vector2 getTop(Ball* gameBall);
			Vector2 getBottom(Ball* gameBall);


			void drawBall();                                       //Dibuja la pelota en pantalla

			void resetInGameBall();                                //Resetea la pelota si se va de los l�mites de la pantalla
			void resetBall();                                      //Resetea la pelota a su origen
		};

		extern float ballSpeedX;
		extern float ballSpeedY;
		extern Vector2 ballSpeed;
		extern Color ballColor;
		extern Ball* gameBall;

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif 