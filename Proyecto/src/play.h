#ifndef PLAY_H
#define PLAY_H

#include "Player.h"
#include "Menu.h"
#include "PowerUps.h"
#include "Ball.h"
#include "screens.h"
#include "game.h"
#include "Buttons.h"


namespace pong {

	namespace play {

		extern bool pause;
		extern bool playing;
		extern int colorCounterP1;
		extern int aux1;
		extern int colorCounterP2;
		//Activa el timer que cuenta los segundos que el powerUp aparece en pantalla
		extern bool activateTimerpowerUp;

		//Dice cual fue la ultima raqueta que toco para darle a esa raqueta el power up
		extern int lastRacketTouched;

		extern bool isPowerUpActive;

		//Game timer
		extern int timer;
		extern int timerSeconds;
		extern int AuxTimerSeconds;
		extern int timerMax;

		//En que segundo se activa la logica y se muestra por pantalla el powerUp
		extern int activationTime;

		//Para poder entrar de nuevo a la funcion de seleccion random de powerUp
		extern bool puSelected;


		extern int powerUpSelected;

		extern bool isActive;

		//Cada cuanto tiempo el powerUp aparece en pantalla
		extern int powerUpOn;

		extern bool powerOn;

		extern bool currentPowerUpOff;

		//Timer PowerUp
		extern int timerPU;
		extern int timerSecondsPU;
		extern int timerMaxPU;

		//Si se toco
		extern bool powerUpTouched;

		extern int gameRounds;
		extern int maxPointSelected;

		//Tiempo que el powerUp estara activo al agarrarlo
		extern int powerUpActiveTime;

		//Timer cuando se agarra el powerUp
		extern int timerPickedPU;
		extern int timerPickedPUCountDown;
		extern int timerSecondsPickedPU;
		extern int timerMaxPickedPU;

		extern int powerUpShowTimeCountDown;

		extern bool timerAuxSeconds;

		extern int changed;

		extern int colorTextPosX;
		extern int colorTextPosY;

		extern int impact;

		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif
